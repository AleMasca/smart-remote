**Smart Remote**

*Nome Progetto*: Smart Remote

*Autore*: Alessandro Mascaretti

*Descrizione*: Il telecomando in questione e' basato su ESP32. Grazie all'utilizzo di uno script di shell o di un assistente vocale per smartphone (ancora da valutare quale possa essere la scelta migliore tra le 2),
verranno mandati dei comandi MQTT al telecomando, il quale dopo averli opportunamente interpretati, li trasformera' in segnale a infrarossi per interagire con la TV. Il telecomando sara' inoltre dotato 
di una fotoresistenza per il rilevamento della luminosita' ambientale. Qualore l'utente stesse vedendo la TV al buio, il telecomando in automatico mandera' un comando
MQTT in modo che eventuali lampade, collegate allo stesso server MQTT e poste dietro alla TV, verranno accese. Ai fini del progetto le lampade verranno simulate con uno o due led.

*Hardware* (provvisorio, potrebbe variare e/o aumentare in corso di sviluppo):
* ESP32
* IR Sender
* IR Receiver
* Led
* Fotoresistenza

Nota: il circuito verra' saldato su millefori e non lasciato volante su breadboard, in quanto verra' poi anche effettivamente utlizzato in casa.

*Link a repo* : [](url)https://gitlab.com/AleMasca/smart-remote (In Allestimento. Verra' modificato e riempito in fase di sviluppo)

*Licenza scelta* : GPLv3